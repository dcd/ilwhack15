#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      MasterMan
#
# Created:     19/02/2015
# Copyright:   (c) MasterMan 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    import json
    data=json.load(file("cache\\cached_data.json"))
    for d in data["data"]:
        print d["securityData"].keys()
    print
    pass

if __name__ == '__main__':
    main()
