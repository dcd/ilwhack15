#!/usr/bin/env python2

# bloomberg_api.py
#
# functions to retrieve Bloomberg data

import argparse
import json
import ssl
import sys
import urllib2

import dateutil.parser
import copy

import re
import pandas

BLOOMBERG_ENDPOINT="http-api.openbloomberg.com"

def get_bloomberg_data(request_data):
    """
        Returns a dict with the loaded JSON from Bloomberg based on the request_data
    """
    print "Connecting to Bloomberg endpoint..."

    req = urllib2.Request('https://{}/request/blp/refdata/HistoricalData'.format(BLOOMBERG_ENDPOINT))
    req.add_header('Content-Type', 'application/json')

    ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    ctx.load_verify_locations('bloomberg.crt')
    ctx.load_cert_chain('client.crt', 'client.key')

    try:
        res = urllib2.urlopen(req, data=json.dumps(request_data), context=ctx)
        data=res.read()
        # this is an awkward fix to the Bloomberg API returning ill-formed JSON !!!
##        data=re.sub(r",\s*?,",",",data)
##        data=re.sub(r",\s*?,",",",data)
##        data=re.sub(r",\s*?,",",",data)
    except Exception as e:
        e
        print e
        return None
    f=file("raw_data.txt","w")
    f.write(data)
    f.close()

    return json.loads(data)


def get_sequential_bloomberg_data(request_data):
    """
        Runs the request separately for each security to avoid saturating the endpoint
    """
    data=None
    this_run=copy.deepcopy(request_data)
    for security in request_data["securities"]:
        if re.match(r"[\w]*?\.B",security):
            continue

        this_run["securities"]=[security]
        this_run_data=get_bloomberg_data(this_run)
        if "securityError" not in this_run_data["data"][0]["securityData"] and \
        len(this_run_data["data"][0]["securityData"]["fieldData"]) > 0:
            if not data:
                data=this_run_data
            else:
                data["data"].extend(this_run_data["data"])

    return data


def get_api_fields(search_string):
    """
        Returns a list of all Fields in API
    """

    request_data = {
        "searchSpec": search_string
    }

    req = urllib2.Request('https://{}/request/blp/apiflds/FieldSearch'.format(BLOOMBERG_ENDPOINT))
    req.add_header('Content-Type', 'application/json')

    ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    ctx.load_verify_locations('bloomberg.crt')
    ctx.load_cert_chain('client.crt', 'client.key')

    try:
        res = urllib2.urlopen(req, data=json.dumps(request_data), context=ctx)
        data=res.read()
    except Exception as e:
        e
        print e
        return None
    f=file("raw_data_fields.txt","w")
    f.write(data)
    f.close()

    return json.loads(data)


def get_field_value(securities,field):
    """
    """
    request_data = {
        "securities": securities,
        "fields": [field]
    }

    req = urllib2.Request('https://{}/request/blp/refdata/ReferenceData'.format(BLOOMBERG_ENDPOINT))
    req.add_header('Content-Type', 'application/json')

    ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    ctx.load_verify_locations('bloomberg.crt')
    ctx.load_cert_chain('client.crt', 'client.key')

    try:
        res = urllib2.urlopen(req, data=json.dumps(request_data), context=ctx)
        data=res.read()
    except Exception as e:
        e
        print e
        return None
##    f=file("raw_data_fields.txt","w")
##    f.write(data)
##    f.close()

    return json.loads(data)

def load_dates(data):
    """
        Converts dates in text format into python datetime
    """
    for entry in data:
        if entry.has_key("date"):
            entry["date"]=dateutil.parser.parse(entry["date"])

def export_api_fields_to_csv(fields_text):
    """
    """
    rx=re.compile(r'\{"id":"(.*?)","fieldInfo":\{"mnemonic":"(.+?)","description":"(.+?)","datatype":"(.+?)","categoryName":\[(.*?)\],"property":\[(.*?)\],"overrides":\[(.*?)\],"ftype":"(.+?)"\}\}',flags=re.IGNORECASE|re.DOTALL)

    res=[]
    for match in rx.finditer(fields_text):
        entry={
        "id":match.group(1),
        "mnemonic":match.group(2),
        "description":match.group(3),
        "datatype":match.group(4),
        "categoryName":match.group(5),
        "property":match.group(6),
        "overrides":match.group(7).replace(","," "),
        "ftype":match.group(8)
            }
        res.append(entry)

    df=pandas.DataFrame(res)
    df.to_csv("bloomberg_fields.csv")

def main():
##    data=get_bloomberg_data(request_data)
##    cPickle.dump(data,file("cached_data.pic","w"))
##    data=get_api_fields("*")
##    print data

##    f=file("raw_data_fields.txt","r")
##    data=f.read()
##    export_api_fields_to_csv(data)

    print get_field_value(["AAPL US Equity","IBM US Equity"],"PX_YEST_LOW_PRE")
    pass

if __name__ == "__main__":
    sys.exit(main())
