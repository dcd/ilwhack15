#!/usr/bin/env python2

# usage: python HistoricalDataRequest.py <host-ip>

import argparse
import json
import ssl
import sys
import urllib2

import pandas
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, WeekdayLocator,\
     DayLocator, MONDAY, date2num
from matplotlib.finance import quotes_historical_yahoo_ohlc, candlestick_ohlc
import dateutil.parser
import cPickle

BLOOMBERG_ENDPOINT="http-api.openbloomberg.com"

request_data = {
    "securities": ["IBM US Equity", "AAPL US Equity"],
    "fields": ["PX_LAST", "OPEN", "EPS_ANNUALIZED"],
    "startDate": "20120101",
    "endDate": "20120301",
    "periodicitySelection": "DAILY"
}

def get_bloomberg_data(request_data):
    """
        Returns a dict with the loaded JSON from Bloomberg based on the request_data
    """
    req = urllib2.Request('https://{}/request/blp/refdata/HistoricalData'.format(BLOOMBERG_ENDPOINT))
    req.add_header('Content-Type', 'application/json')

    ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    ctx.load_verify_locations('bloomberg.crt')
    ctx.load_cert_chain('client.crt', 'client.key')

    try:
        res = urllib2.urlopen(req, data=json.dumps(request_data), context=ctx)
        data=res.read()
    except Exception as e:
        e
        print e
        return None
    return json.loads(data)


def plot_candles(plot_data):
    """
        Make candle plot out of Bloomberg data
        Needs OHLC (Open, High, Low, Close)
    """

    # (Year, month, day) tuples suffice as args for quotes_historical_yahoo
    date1 = (2004, 2, 1)
    date2 = (2004, 4, 12)


    mondays = WeekdayLocator(MONDAY)        # major ticks on the mondays
    alldays = DayLocator()              # minor ticks on the days
    weekFormatter = DateFormatter('%b %d')  # e.g., Jan 12
    dayFormatter = DateFormatter('%d')      # e.g., 12

##    quotes = quotes_historical_yahoo_ohlc('INTC', date1, date2)
##    if len(quotes) == 0:
##        raise SystemExit

    plot_data.plot()

    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.2)
    ax.xaxis.set_major_locator(mondays)
    ax.xaxis.set_minor_locator(alldays)
    ax.xaxis.set_major_formatter(weekFormatter)
    #ax.xaxis.set_minor_formatter(dayFormatter)

##    quotes=zip((plot_data["date"].dt.year,plot_data["date"].dt.month,plot_data["date"].dt.day),plot_data["OPEN"],plot_data["OPEN"], plot_data["PX_LAST"],plot_data["PX_LAST"])

    quotes=zip([date2num(x) for x in plot_data["date"]],plot_data["OPEN"],plot_data["OPEN"], plot_data["PX_LAST"],plot_data["PX_LAST"])

    #plot_day_summary(ax, quotes, ticksize=3)
##    candlestick_ohlc(ax, quotes, width=0.6)
    candlestick_ohlc(ax, quotes, width=0.6)

    ax.xaxis_date()
    ax.autoscale_view()
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')

def load_dates(data):
    for entry in data:
        if entry.has_key("date"):
            entry["date"]=dateutil.parser.parse(entry["date"])

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('host', type=str)
##    data=get_bloomberg_data(request_data)
##    cPickle.dump(data,file("cached_data.pic","w"))

    data=cPickle.load(file("cached_data.pic","r"))

    if not data:
        print "Error connecting to endpoint"
        return

    for securityData in data["data"]:
        securityData=securityData["securityData"]
        if securityData["security"]=="IBM US Equity":
            plot_data=securityData["fieldData"]
            load_dates(plot_data)
            df=pandas.DataFrame(plot_data)
##    df.plot()
    plot_candles(df)
    plt.show()

if __name__ == "__main__":
    sys.exit(main())
