#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      MasterMan
#
# Created:     18/02/2015
# Copyright:   (c) MasterMan 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import pandas
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, WeekdayLocator,\
     DayLocator, MONDAY, date2num
from matplotlib.finance import quotes_historical_yahoo_ohlc, candlestick_ohlc
import cPickle
import json

from itertools import combinations

from bloomberg_api import get_bloomberg_data, get_sequential_bloomberg_data,load_dates, get_field_value


def plot_candles(plot_data):
    """
        Make candle plot out of Bloomberg data
        Needs OHLC (Open, High, Low, Close)
    """

    # (Year, month, day) tuples suffice as args for quotes_historical_yahoo
    date1 = (2004, 2, 1)
    date2 = (2004, 4, 12)

    mondays = WeekdayLocator(MONDAY)        # major ticks on the mondays
    alldays = DayLocator()              # minor ticks on the days
    weekFormatter = DateFormatter('%b %d')  # e.g., Jan 12
    dayFormatter = DateFormatter('%d')      # e.g., 12

##    quotes = quotes_historical_yahoo_ohlc('INTC', date1, date2)
##    if len(quotes) == 0:
##        raise SystemExit

    plot_data.plot()

    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.2)
    ax.xaxis.set_major_locator(mondays)
    ax.xaxis.set_minor_locator(alldays)
    ax.xaxis.set_major_formatter(weekFormatter)
    #ax.xaxis.set_minor_formatter(dayFormatter)

##    quotes=zip((plot_data["date"].dt.year,plot_data["date"].dt.month,plot_data["date"].dt.day),plot_data["OPEN"],plot_data["OPEN"], plot_data["PX_LAST"],plot_data["PX_LAST"])

    quotes=zip([date2num(x) for x in plot_data["date"]],plot_data["OPEN"],plot_data["OPEN"], plot_data["PX_LAST"],plot_data["PX_LAST"])

    #plot_day_summary(ax, quotes, ticksize=3)
##    candlestick_ohlc(ax, quotes, width=0.6)
    candlestick_ohlc(ax, quotes, width=0.6)

    ax.xaxis_date()
    ax.autoscale_view()
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')

def load_SecurityData(data):
    """
        Loads the values from JSON into pandas dataframes
    """
    df={}

    for securityData in data["data"]:
        securityData=securityData["securityData"]
        prev_df=df.get(securityData["security"],pandas.DataFrame())
        plot_data=securityData["fieldData"]
        load_dates(plot_data)
        new_df=pandas.DataFrame(plot_data)
        df[securityData["security"]]=pandas.concat([prev_df,new_df])

    return df

def get_security_comparison(dataframes,field):
    """
        Returns a pandas DataFrame where for each data point, each column is the
        same field for a different security

        It is understood this is for the same date interval
    """
    res=None
    for security in dataframes:
        print security
        index=dataframes[security].columns.get_loc(field)
        old_columns=list(dataframes[security].columns.values)
        new_columns=list(dataframes[security].columns.values)
        new_name=security#+" "+field
        new_columns[new_columns.index(field)]=new_name
        dataframes[security].columns=new_columns
        new_df=dataframes[security][["date",new_name]]
        if res is None:
            res=new_df
        else:
            res=pandas.merge(res,new_df)
        dataframes[security].columns=old_columns
    return res

def stats_indicators(combi,df):
    """
        Computes correlation, covariance, etc. between 2 given fields of a
        DataFrame
    """
    corr=df[[combi[0],combi[1]]].corr() #method="spearman"
    cov=df[[combi[0],combi[1]]].cov()
    return {"corr":corr.loc[combi[0]][combi[1]],"cov":cov.loc[combi[0]][combi[1]]}

def export_force_directed_graph(fields,df,company_data=None):
    """
    """
    cd=company_data["all_data"] if company_data else None
    nodes=[]
    for field in fields:
        new_node={"name":field.split()[0], "group":200}
        if company_data:
            sector=company_data["all_data"][cd["Ticker symbol"] == field]["GICS Sector"].values[0]
            new_node["group"]=company_data["sectors"].index(sector)+1
        nodes.append(new_node)

##    nodes=[{"name":field,"group":company_data["sectors"].index(company_data["all_data"].loc[company_data["all_data"]["Ticker symbol"] == field])+1} for index,field in enumerate(fields)]
    links=[]

    combis=combinations(fields,2)
    for combi in combis:
##        print "Correlation between ",combi[0],"and",combi[1],"on",field,":", df1[combi[0]][[field]].corr(df1[combi[1]][[field]])
        print "Between ",combi[0],"and",combi[1],"on",field,":"
        stats=stats_indicators(combi,df)
        print "Correlation:",stats["corr"]
        print "Covariance:",stats["cov"]
##        if stats["corr"] >= 0.5:
        link_value=float((1-abs(stats["corr"])))*20
        links.append({"source":fields.index(combi[0]),"target":fields.index(combi[1]),"value":link_value})
    return {"nodes":nodes,"links":links}

def load_financial_data(filename):
    """
        Loads a spreadsheet with data about companies
    """
    data=pandas.read_csv(filename)
    print data.columns
    sectors=list(set(data["GICS Sector"]))
    symbols=pandas.Series([sym+" US Equity" for sym in data["Ticker symbol"]])
    data["Ticker symbol"]=symbols
##    print data
##    print symbols,sectors
    return {"all_data":data,"sectors":sectors}
##    print sp500["GICS Sector"].head()


def download_sp500():
    """
    """
    company_data=load_financial_data("sp500.csv")
    request_data = {
##        "securities": ["IBM US Equity", "AAPL US Equity"],
        "securities": list(company_data["all_data"]["Ticker symbol"][:50]),
        "fields": ["PX_LAST"],
##        "fields": ["PX_LAST", "OPEN", "EPS_ANNUALIZED"],
        "startDate": "20140101",
        "endDate": "20141231",
        "periodicitySelection": "DAILY"
    }

    LOAD_DATA=True
    # uncomment the 2 lines below to send the request to bloomberg instead of loading from the cache
    if LOAD_DATA:
        data=get_sequential_bloomberg_data(request_data)
        json.dump(data,file("cache\\cached_data.json","w"))
    else:
        data=json.load(file("cache\\cached_data.json","r"))

    if not data:
        print "Error connecting to endpoint"
        return

    df1=load_SecurityData(data)
    field="PX_LAST"
    df2=get_security_comparison(df1,field)

    fdg=export_force_directed_graph(df1.keys(),df2,company_data)
    json.dump(fdg,file("correlations.json","w"))


def do_cool_companies():
    """
        Quick copy-paste of sp500
    """
    stock_data=pandas.read_csv("interesting_stocks.csv")
    stocks=[stock+" US Equity" for stock in stock_data["Ticker symbol"]]
    company_data=load_financial_data("sp500.csv")

##    for stock in stocks:
##        if stock not in company_data["all_data"]["Ticker symbol"]:
##            print stock

    request_data = {
        "securities": stocks,
        "fields": ["PX_LAST"],
        "startDate": "20140101",
        "endDate": "20141231",
        "periodicitySelection": "DAILY"
    }

    LOAD_DATA=False
    if LOAD_DATA:
        data=get_sequential_bloomberg_data(request_data)
        json.dump(data,file("cache\\cached_data.json","w"))
    else:
        data=json.load(file("cache\\cached_data.json","r"))

    if not data:
        print "Error connecting to endpoint"
        return

    df1=load_SecurityData(data)
    field="PX_LAST"
    df2=get_security_comparison(df1,field)

    fdg=export_force_directed_graph(df1.keys(),df2)
    json.dump(fdg,file("correlations.json","w"))

def test_interesting_fields():
    """
    """
    stock_data=pandas.read_csv("interesting_stocks.csv")
    stocks=[stock+" US Equity" for stock in stock_data["Ticker symbol"]]
##    stocks=["IBM US Equity", "AAPL US Equity"]

    static_fields=["TOT_SALARIES_PAID_TO_CEO_&_EQUIV", "BOARD_AVERAGE_AGE", "%_WOMEN_ON_BOARD", "PX_YEST_OPEN_PRE", "PX_YEST_OPEN_POST", "PX_YEST_LOW_PRE", "PX_YEST_LOW_POST", "PX_YEST_HIGH_PRE", "PX_YEST_HIGH_POST", "PX_YEST_BID_PRE", "PX_YEST_BID_POST", "PX_YEST_BID_ALL", "PX_YEST_ASK_PRE", "PX_YEST_ASK_POST", "PX_YEST_ASK_ALL", "PX_CLOSE_5D_PRE", "PX_CLOSE_5D_POST", "CHG_NET_5D_PRE", "CHG_NET_5D_ALL", "CHG_NET_5D_POST", "DATE_OF_LAST_BOD_CHANGE", "LAST_BOARD_START_DATE", "BEST_EPS_NUMEST", "MOV_AVG_20D", "TOTAL_VOTING_SHARES_VALUE", "EMPL_GROWTH", "5YR_AVG_RETURN_ON_ASSETS", "TWITTER_SENTIMENT_REALTIME", "MOV_AVG_180D", "BS_TOTAL_NON_CURRENT_ASSETS", "BS_DERIVATIVE_&_HEDGING_LIABS_ST"]

    res=[]
    chunk_size=10
    for field in static_fields:
        print field
        fdict={"field":field}
##        for stock in stocks:
        for chunk in range(0,len(stocks),chunk_size):
            data=get_field_value(stocks[chunk:chunk+chunk_size],field)
            for data_dict in data["data"][0]["securityData"]:
                for field in data_dict["fieldData"]:
                    fdict[data_dict["security"]]=data_dict["fieldData"][field]
        res.append(fdict)

    df=pandas.DataFrame(res)
    df.to_csv("STATIC_FIELDS.csv")

    if not data:
        print "Error connecting to endpoint"
        return

##    print data

def plot_bubbles():
    """
    """
    data=pandas.read_csv("STATIC_FIELDS.csv")
    df=data[["TOT_SALARIES_PAID_TO_CEO_&_EQUIV","BOARD_AVERAGE_AGE","%_WOMEN_ON_BOARD"]]
    df.plot(kind='scatter', x="BOARD_AVERAGE_AGE",y="TOT_SALARIES_PAID_TO_CEO_&_EQUIV", s=df["%_WOMEN_ON_BOARD"]*20, c=df["%_WOMEN_ON_BOARD"]*10)
    print df[["TOT_SALARIES_PAID_TO_CEO_&_EQUIV","BOARD_AVERAGE_AGE"]].corr(method="pearson")
    plt.show()

def main():


##    test_interesting_fields()
##    print df2
##    df2.plot(title=field)
##    plot_candles(df)
##    plt.show()
##    plot_bubbles()
##    do_cool_companies()
    download_sp500()
    pass

if __name__ == '__main__':
    main()
