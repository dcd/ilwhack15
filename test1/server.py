#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      MasterMan
#
# Created:     18/02/2015
# Copyright:   (c) MasterMan 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import pandas
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, WeekdayLocator,\
     DayLocator, MONDAY, date2num
from matplotlib.finance import quotes_historical_yahoo_ohlc, candlestick_ohlc
import dateutil.parser
##import cPickle

from bloomberg_api import get_bloomberg_data

request_data = {
    "securities": ["IBM US Equity", "AAPL US Equity"],
    "fields": ["PX_LAST", "OPEN", "EPS_ANNUALIZED"],
    "startDate": "20120101",
    "endDate": "20120301",
    "periodicitySelection": "DAILY"
}

def main():
    pass

if __name__ == '__main__':
    main()
